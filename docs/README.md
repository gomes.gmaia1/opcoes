**DEFINIÇÃO DO PROBLEMA.** Estudar a relação entre dados de séries temporais de ações diárias em mercado aberto e variáveis que descrevem o mercado de opções baseados em ativos derivativos desses subjacentes.

A literatura tem modelado este tipo de problema de diversas formas, utilizando séries temporais, utilizando regressão, utilizando redes neurais, mas não como um problema de estimação de curvas de demanda para cada um dos tipos de contrato de opções onde, baseado nas curvas de demanda, colocamos este resultado em um problema de classificação para indicar o movimento do preço de sua ação de referência. Entretanto, problemas como **dimensionalidade** dos dados e **heterogeneidade** nos gostos dos consumidores são desafios a serem superados na modelagem de curvas de demanda.

**Dimensionalidade.** No estudo de mercados com um grande número de produtos e, por consequência, um grande número de parâmetros a serem estimados, temos um problema de dimensionalidade dos dados. O modelo logito de demanda (McFadden, 1973) resolve o problema da dimensionalidade ao projetar os produtos no espaço das características.

**Heterogeneidade.** Também, o modelo de estimação de curvas de demanda para produtos diferenciados (tipos de contrato de opções) busca não restringir a heterogeneidade presente pelo gosto do consumidor (investidor). Um modo seria propor algum modelo que explicitamente explique a heterogeneidade presente na população ao se estimar parâmetros que governem a mesma. Entretanto, este modelos tratam os regressores, incluindo preços como exôgenos ao problema, o que se torna especialmente problematico ao se usar dados agregados para estimar o modelo (Nevo, 2016). Minha sugestão seria adequar a realidade do problema a um modelo que utilize a vantagem do modelo logito em tratar um grande número de tipos de contratos mas que também lide com o problema da endogeneidade do preço.

#### O modelo para curvas de demandas (Baseado no BLP, Pakes)

**Primeiro.** O grande beneficio deste modelo é que não necessitamos de informação de investidores individuais, levando em consideração apenas dados de preços e de quantidades para cada contrato.

**Segundo.** A estimação permite que os preços estejam correlacionados com o termo de erro. Para modelar deste forma, consideramos que quem está vendendo e quem está comprando opções conhecem todas as características que movimentam o mercado delas (o que é razoável do ponto de vista de dados agregados). O pesquisador, entretanto, é assumido de observar apenas algumas das características, ou seja, assumimos que para cada contrato de opção temos alguma variável que influencia na demanda da mesma mas que não foi observada pelo pesquisador ou não pode ser quantificada de tal modo a entrar na análise. Assim, as características não observadas serão capturadas pelo termo de erro e como os investidores de compra e venda de opções são assumidos de conhecer todas as características das opções, eles levam isto em consideração ao escolherem o preço, logo, tornando o preço endógeno ao problema.

#### Aplicando o modelo ao mercado de opções

Definir cada dia em $`t = 1,...,T`$ como sendo um mercado em que $`i = 1,...,I`$ represente cada hora em que o mercado esteve aberto (BLP define como consumidor $`i`$). Pra cada mercado, observamos quantidades, médias de preço e de características para os $`J`$ tipos de contratos de opções.

A utilidade indireta de se comprar (ou vender) a opção $`j`$ no mercado $`t`$ é igual à $`U(\chi_{jt},\zeta_{jt}, \rho_{jt}, \tau_{i}; \theta)`$. Onde,

* **$`\chi_{jt}`$** Características observadas pelo pesquisador acerca das opções;
* **$`\zeta_{jt}`$** Características não observadas pelo pesquisador acerca das opções;
* **$`\rho_{jt}`$** Preço;
* **$`\tau_{i}`$** Características individuais acerca da hora $`i`$;
* **$`\theta`$** Parâmetros desconhecidos

Adaptando nosso caso particular à especificação de utilidade indireta de Nevo, 2016, sugerimos utilizar o volume de transações em detrimento a renda do indice *i*. Portanto,

$`u_{ijt} = \alpha_i(y_i-p_{jt}) + \chi_{jt}\beta_i + \zeta_{jt} + \epsilon_{ijt}`$

onde,

* **$`y_i`$** Volume de movimentação na hora $`i`$;
* **$`\chi_{jt}`$** É um vetor K-dimensional de características observadas pelo pesquisador acerca da opção $`j`$;
* **$`\zeta_{jt}`$** Características não observadas pelo pesquisador acerca das opções;
* **$`\rho_{jt}`$** Preço da opção $`j`$ no dia $`t`$;
* **$`\epsilon_{ijt}`$** Termo estocástico com média zero;
* **$`\alpha_i`$** Utilidade marginal da hora $`i`$ com relação ao volume;
* **$`\beta_i`$** É um vetor K-dimensional de coeficientes de gostos específicos da hora $`i`$.

**Observação 1.** A equação acima é derivada de uma função de utilidade quasilinear, o que é livre de *wealth effects*. Entretanto, para o caso de mercado de opções, esta não é uma suposição razoável, o que para controlar este efeito, temos que alterar o jeito que o termo $`y_i - p_{jt}`$ entra na função (Petrin,1999).

**Observação 2.** A equação acima especifica as características não observadas, que entre outras funções capta os elementos de diferenciação de produtos verticais (contratos de opções), como identicas para todas as horas do dia.

**Observação 3.** A equação acima assume que todas as horas do dia de mercado aberto estão expostas às mesmas características dos produtos (contrato de opções), principalmente, preço. O que motiva um precedimento de variável instrumental onde busca-se descrever as variações de preferências de cada hora como função de características individuais $`\tau_{i}`$, ou seja, modelar os gostos relativos a cada hora do dia. Para isso, as características individuais são divididas em duas componentes, $`D_i`$ e $`v_i`$, responsáveis pelas características *observadas* e *não observadas*, respectivamente. E prossegue-se a modelagem:

$`\begin{pmatrix}
 \alpha_i \\
 \beta_i
\end{pmatrix} =
\begin{pmatrix}
 \alpha \\
 \beta
\end{pmatrix} + \Pi D_i + \Sigma v_i, \text{com } v_i \sim P^*_v(v), D_i \sim \hat{P_D^*}(D)`$

Onde $`D_i`$ é um vetor $`d \times 1`$ de variáveis observadas, enquanto $`v_i`$ captura as informações adicionais discutidas anteriormente. $`P_v^*(.)`$ é uma função paramêtrica enquanto que $`\hat{P_D^*}(D)`$ pode ser tanto uma distribuição paramétrica quanto uma distribuição não paramétrica. $`\Pi`$ é uma matriz $`(K+1)\times d`$ de coeficientes que medem como características de preferência de cada hora variam de acordo com as variáveis observadas de $`D`$. Por último, temos que $`\Sigma`$ é uma matriz $`(K+1)\times(K+1)`$ de parâmetros.

Se assumirmos que $`P^*v(.)`$ é uma distribuição normal multivariada (Nevo, 2016), então a matriz $`\Sigma`$ permite que cada componente de $v_i$ tenha variância diferente dos demais bem como permite correlação entre as características.

Por simplicidade, podemos também assumir que $`v_i`$ e $`D_i`$ são independentes.

**Vantagens de se permitir que parâmetros de gostos possam variar com relação as variáveis observadas $`D_i`$:**

* **Vantagem 1.** Permite que se inclua informações adicionais na análise;
* **Vantagem 2.** Diminui a necessidade de confiança em suposições paramétricas.

Para terminar a especificação do sistema de demanda, seria interessante, também, a introdução da opção de *não compra*, ou seja, que aquela hora do dia talvez decida não comprar nada. Para a utilidade indireta da *não compra*, temos:

$`u_{i0t} = \alpha_i y_i  + \zeta_{0t} +  \Pi_0 D_i + \sigma_0 v_{i0} + \epsilon_{i0t}`$

Temos que, 

**1.** $`\zeta_{0t}`$ não é identificável sem que sejam feitas maiores suposições;

**2.** $`\Pi_{0}`$ e $`\sigma_{0}`$ também não são identificáveis separadamente do termo constante de especificade individual. 

Portanto, a prática padrão é setarmos $`\zeta_{0t}`$, $`\Pi_{0}`$ e $`\sigma_{0}`$ iguais à zero e, como o termo $`\alpha_i y_i`$ vai eventualmente sumir (porque é comum a todos os tipos de contrato), isto é equivalente a normalizar a utilidade de *não compra* igual à zero.

Logo, fazendo $`\theta = (\theta_1,\theta_2)`$ ser o vetor contendo todos os parâmetros do modelo, temos que $`\theta_1 = (\alpha, \beta)`$ contêm os parâmetros *lineares* e o vetor $`\theta_2 = (\Pi, \Sigma)`$ contêm os *não-lineares*. Combinando todo o exposto até agora, temos:

**(A)** $`u_{ijt} = \alpha_i y_i + \delta_{jt}(\chi_{jt},\rho_{jt}, \zeta_{jt};\theta_1) + u_{ijt}(\chi_{jt},\rho_{jt}, v_i, D_{i};\theta_2) + \epsilon_{ijt},`$ 

**(B)** $`\delta_{jt}=\chi_{jt}\beta -  \alpha  \rho_{jt} +  \zeta_{jt}  \text{, e}`$

**(C)** $` u_{ijt} = [-\rho_{jt}, \chi_{jt}](\Pi D_{i} + \Sigma v_i)`$

onde $`[-\rho_{jt}, \chi_{jt}]`$ é um vetor $`1 \times (K+1)`$. Portanto a utilidade indireta é agora expressa como a soma de 4 termos. 

* $`\alpha_i y_i`$ vai sumir e é apresentado apenas para consistência do modelo. 
* Já o segundo termo ,$`\delta_{jt}`$, se refere a utilidade média comum a todas as horas do dia. 
* Por último, os últimos dois termos $`u_{ijt} + \epsilon_{ijt}`$ representam os desvios de média zero heterocedasticos da útilidade média qie capturam os efeitos dos coeficientes aleatórios.

Logo, podemos implicitamente definir um conjunto de atributos individuais que levam a escolha do contrato $`j`$. Nevo em *A practioner's guide to estimation of Random Coefficients Logit Models of Demand* define formalmente,

```math
A_{jt}(\chi_t,\rho_t,\delta_t;\theta_2) = \{ (D_i, v_i, \epsilon_{i0t},...,\epsilon_{iJt})|u_{ijt} \geq u_{ilt} \forall l = 0,1,...,J\}
```
onde,

* $`\chi_t = (\chi_{lt},...,\chi_{Jt})'`$: são características *observáveis*;
* $`\rho_t = (\rho_{lt},...,\rho_{Jt})'`$: são preços;
* $`\delta_t = (\delta_{lt},...,\delta_{Jt})'`$: são utilidades médias.

**Conclusão.** O conjunto $`A_{jt}`$ define os individuos que escolhem o contrato *J* no dia *t*. Com este resultado, como os contratos de opção estão intimamente relacionados com os valores esperados da ação na data do vencimento da opção e todos entregam o mesmo produto (a preços diferentes), surge o questionamento se não poderíamos utilizar as curvas de demanda por opções para entendermos o teor do mercado do ativo que estes contratos negociam. Uma sugestão seria, por exemplo, adptarmos a um problema de classificação acerca da opção mais "desejada" pelas curvas de demanda criadas.
