# Repositório do Projeto

* **Bibliografia.** Contêm artigos e reports de interesse
* **Docs.** Documentos gerados ao longo do processo
* **Dados.** Pasta com os dados utilizados
* **Scripts.** Pasta com todos os scripts utilizados

